Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'

  root 'healthcheck#show'

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :users, only: %i[create]
      resources :credits, only: %i[index]
      get 'credit' => 'credits#show'
      get 'user' => 'users#show'
      get 'referral_code' => 'referral_codes#show'

      scope :auth do
        post 'token' => 'user_token#create'
      end
    end
  end
end
