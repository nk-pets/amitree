class AddDefaultStatusToReferral < ActiveRecord::Migration[6.1]
  def change
    change_column_default :referrals, :status, 'created'
  end
end
