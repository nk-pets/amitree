class CreateReferrals < ActiveRecord::Migration[6.1]
  def change
    create_table :referrals do |t|
      t.references :user, null: false, foreign_key: true
      t.references :referral_code, null: false, foreign_key: true
      t.string :status

      t.timestamps
    end
  end
end
