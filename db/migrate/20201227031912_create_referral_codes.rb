class CreateReferralCodes < ActiveRecord::Migration[6.1]
  def change
    create_table :referral_codes do |t|
      t.references :user, null: false, foreign_key: true
      t.string :code
      t.integer :invited

      t.timestamps
    end
  end
end
