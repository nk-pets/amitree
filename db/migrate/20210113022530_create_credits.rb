class CreateCredits < ActiveRecord::Migration[6.1]
  def change
    create_table :credits do |t|
      t.references :user, null: false, foreign_key: true
      t.integer :balance
      t.integer :amount
      t.string :reason
      t.references :referral, foreign_key: true

      t.timestamps
    end
  end
end
