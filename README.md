## Amitree Take Home Technical Challenge

1. [Application Requirements](docs/01-application-requirements.md)
2. [Architecture](docs/02-architecture.md)
3. [Code Architecture](docs/03-code-architecture.md)
4. [Development](docs/04-development.md)
5. [Deployment](docs/05-deployment.md)
