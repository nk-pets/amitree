FROM ruby:2.7.2-alpine AS build-env

ARG RAILS_ROOT=/rails
ARG BUILD_PACKAGES="build-base curl-dev git"
ARG DEV_PACKAGES="postgresql-dev yaml-dev zlib-dev"
ARG RUBY_PACKAGES="tzdata"

ENV BUNDLE_APP_CONFIG="$RAILS_ROOT/.bundle"
ENV BUNDLE_CACHE_ALL="true"

WORKDIR $RAILS_ROOT

RUN apk update \
    && apk upgrade \
    && apk add --no-cache --virtual .build-deps $BUILD_PACKAGES \
    && apk add --update --no-cache $DEV_PACKAGES $RUBY_PACKAGES

COPY Gemfile* $RAILS_ROOT/
COPY ./vendor/ $RAILS_ROOT/vendor

RUN bundle config --global frozen 1 \
    && bundle install --jobs $(nproc) --retry 3 --path=vendor/bundle

# RUN apk --purge del .build-deps

COPY . $RAILS_ROOT

FROM ruby:2.7.2-alpine

ARG RAILS_ROOT=/rails
ARG PACKAGES="tzdata postgresql-client"

ENV BUNDLE_APP_CONFIG="$RAILS_ROOT/.bundle"
ENV BUNDLE_CACHE_ALL="true"

WORKDIR $RAILS_ROOT

RUN apk update \
    && apk upgrade \
    && apk add --update --no-cache $PACKAGES

COPY --from=build-env $RAILS_ROOT $RAILS_ROOT

EXPOSE 5000
CMD ["bundle", "exec", "rails", "s", "-b", "0.0.0.0", "-p", "5000"]
