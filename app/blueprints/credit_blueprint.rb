class CreditBlueprint < Blueprinter::Base
  identifier :id

  fields :balance, :amount
end
