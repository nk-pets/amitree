class ReferralCodeBlueprint < Blueprinter::Base
  identifier :id

  fields :referral_link
end
