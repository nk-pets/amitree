module RescueResponse
  def self.included(klass)
    klass.class_eval do
      include ApiResponse
      extend ApiResponse
    end
  end

  def routing_error
    api_error title: 'Not found',
              detail: 'Requested route was not found',
              status: :not_found
  end

  def render404
    api_error title: 'Not found',
              detail: 'Requested resource was not found',
              status: :not_found
  end

  def render500(exception)
    raise exception if Rails.env.development?

    api_error title: exception.class.to_s,
              detail: exception.message,
              status: :internal_server_error
  end

  def render_api_exception(exception)
    raise exception if Rails.env.development?

    api_error title: exception.title,
              detail: exception.detail,
              status: exception.status
  end
end
