module KnockOverrides
  def unauthorized_entity(*)
    api_error title: 'Not authorized',
              detail: 'Provided Bearer token is not valid',
              status: :not_found
  end
end
