class ApplicationController < ActionController::API
  include Knock::Authenticable
  include RescueResponse
  include KnockOverrides

  rescue_from StandardError, with: :render500
  rescue_from ApiExceptions::BaseException, with: :render_api_exception
  rescue_from ActiveRecord::RecordNotFound, with: :render404
end
