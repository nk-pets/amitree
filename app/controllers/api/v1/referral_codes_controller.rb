module Api
  module V1
    class ReferralCodesController < ApplicationController
      before_action :authenticate_user, only: [:show]

      def show
        referral_code = ReferralCode.get(current_user)

        api_response payload: ReferralCodeBlueprint.render(referral_code),
                     status: :created
      rescue ActiveRecord::RecordInvalid => e
        api_error title: 'Could not create a referral code',
                  detail: e.message
      end
    end
  end
end
