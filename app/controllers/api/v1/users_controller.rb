module Api
  module V1
    class UsersController < ApplicationController
      before_action :authenticate_user, except: [:create]

      def create
        user = User.register(user_params)

        api_response payload: UserBlueprint.render(user),
                     status: :created
      rescue ActiveRecord::RecordInvalid => e
        api_error title: 'Could not register a new user',
                  detail: e.message
      end

      def show
        api_response payload: UserBlueprint.render(current_user)
      end

      def user_params
        params.require(:user).permit(:email, :name, :password, :password_confirmation, :referral_code)
      end
    end
  end
end
