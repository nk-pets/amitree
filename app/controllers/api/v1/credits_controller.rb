module Api
  module V1
    class CreditsController < ApplicationController
      before_action :authenticate_user, :verify_credits_exist

      def index
        api_response payload: CreditBlueprint.render(current_user.credits)
      end

      def show
        api_response payload: CreditBlueprint.render(current_user.credits.last)
      end

      private

      def verify_credits_exist
        raise ApiExceptions::CreditsError::EmptyError unless current_user.credits.exists?
      end
    end
  end
end
