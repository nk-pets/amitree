module Api
  module V1
    class UserTokenController < Knock::AuthTokenController
      include RescueResponse

      rescue_from StandardError, with: :render500
      rescue_from ActiveRecord::RecordNotFound, with: :render404

      skip_before_action :verify_authenticity_token, raise: false
    end
  end
end
