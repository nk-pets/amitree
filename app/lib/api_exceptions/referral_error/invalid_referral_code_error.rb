module ApiExceptions
  class ReferralError < ApiExceptions::BaseException
    class InvalidReferralCodeError < ApiExceptions::ReferralError
      def initialize
        super

        @title = 'Invalid referral code'
        @detail = 'Please provide a valid referral code'
        @status = :bad_request
      end
    end
  end
end
