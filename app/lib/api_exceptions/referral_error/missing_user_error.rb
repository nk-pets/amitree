module ApiExceptions
  class ReferralError < ApiExceptions::BaseException
    class MissingUserError < ApiExceptions::ReferralError
      def initialize
        super

        @title = 'User for referral missing'
        @detail = 'Please provide a valid user'
        @status = :bad_request
      end
    end
  end
end
