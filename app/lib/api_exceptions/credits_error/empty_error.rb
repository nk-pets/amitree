module ApiExceptions
  class CreditsError < ApiExceptions::BaseException
    class EmptyError < ApiExceptions::CreditsError
      def initialize
        super

        @title = 'Credits are missing'
        @detail = "Current user doesn't have any credits"
        @status = :not_found
      end
    end
  end
end
