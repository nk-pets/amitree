module ApiExceptions
  class BaseException < StandardError
    attr_reader :detail, :status, :title
  end
end
