class ReferralCreditWorker
  include Sidekiq::Worker
  sidekiq_options queue: :referral

  def perform(referral_id)
    referral = Referral.find_by(id: referral_id)
    raise StandardError, "Referral #{referral_id} was not found" if referral.blank?
    raise StandardError, "Referral is already #{referral.status}" unless referral.created?

    Credit.process_referral(referral)
  end
end
