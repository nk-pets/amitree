class Referral < ApplicationRecord
  STATUSES = %w[created processing finished].freeze

  enum status: STATUSES.zip(STATUSES).to_h

  belongs_to :user
  belongs_to :referral_code

  delegate :eligible_for_payout?, to: :referral_code

  # A user who refers another user.
  has_one :referrer, through: :referral_code, source: :user
  has_one :referrer_reward, ->(referral) { where(user: referral.referrer) }, class_name: 'Credit', inverse_of: false

  # A user who was referred by another user.
  alias_attribute :referee, :user
  has_one :referee_reward, ->(referral) { where(user: referral.referee) }, class_name: 'Credit', inverse_of: false

  after_create_commit :process_credit, :update_counters

  class << self
    def link(user, referral_code)
      raise ApiExceptions::ReferralError::MissingUserError if user.blank?

      referral_code = ReferralCode.find_by(code: referral_code)
      raise ApiExceptions::ReferralError::InvalidReferralCodeError if referral_code.blank?

      (referral = new(user_id: user.id, referral_code_id: referral_code.id, status: :created)).save!

      referral
    end
  end

  def referrer_eligible_for_reward?
    processing? && referrer_reward.blank? && eligible_for_payout?
  end

  def referee_eligible_for_reward?
    processing? && referee_reward.blank?
  end

  private

  def update_counters
    ReferralCode.account_for(self)
  end

  def process_credit
    ReferralCreditWorker.perform_async(id)
  end
end
