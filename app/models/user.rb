class User < ApplicationRecord
  has_secure_password

  has_one :referral_code, dependent: :destroy
  has_many :credits, dependent: :destroy
  has_many :referral_payouts, -> { Credit.referral_payouts },
           class_name: 'Credit', inverse_of: false

  has_many :referrals, through: :referral_code

  # Users that were invited by current user
  has_many :referees, through: :referrals, source: :user

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true, format: { with: URI::MailTo::EMAIL_REGEXP }

  class << self
    def register(params)
      transaction do
        (user = new(params.except(:referral_code))).save!

        Referral.link(user, params[:referral_code]) if params.key? :referral_code

        user
      end
    end
  end

  def credit_balance
    credits.last&.balance.to_i
  end
end
