module FrontendUrlConcern
  extend ActiveSupport::Concern

  included do
    def frontend_url(path)
      [ENV['FRONTEND_URL'], path].map(&:to_s).join('/')
    end
  end
end
