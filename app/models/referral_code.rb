class ReferralCode < ApplicationRecord
  include FrontendUrlConcern

  INVITED_REWARD_THRESHOLD = 5

  belongs_to :user

  has_many :referrals, dependent: :destroy
  has_many :referral_payouts, through: :user
  before_create :set_unique_code

  class << self
    def get(user)
      find_or_create_by(user_id: user.id)
    end

    def account_for(referral)
      referral.referral_code.increment(:invited, 1).save!
    end
  end

  def referral_link
    frontend_url(code)
  end

  def eligible_for_payout?
    (invited / INVITED_REWARD_THRESHOLD) > referral_payouts.count
  end

  private

  def set_unique_code
    loop do
      self.code = SecureRandom.hex
      break unless ReferralCode.exists?(code: code)
    end
  end
end
