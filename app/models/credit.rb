class Credit < ApplicationRecord
  belongs_to :user
  belongs_to :referral, optional: true

  REFERRER_REWARD = 1000
  REFEREE_REWARD = 1000

  REASONS = %w[referral_reward referee_reward].freeze

  scope :referral_payouts, -> { where.not(referral_id: nil).where(reason: 'referral_reward') }

  validates :reason, inclusion: { in: REASONS }
  validates :balance, presence: true
  validates :amount, presence: true

  class << self
    def process_referral(referral)
      raise StandardError, 'referral is not an instance of the Referral class' unless referral.is_a? Referral

      transaction do
        referral.processing!
        add_referrer_credit(referral) if referral.referrer_eligible_for_reward?
        add_referee_credit(referral) if referral.referee_eligible_for_reward?
        referral.finished!
      end
    end

    def add(user, amount, reason, referral_id = nil)
      last_balance = where(user_id: user.id).last&.balance.to_i

      create(user: user, amount: amount, reason: reason, balance: last_balance + amount, referral_id: referral_id)
    end

    private

    def add_referrer_credit(referral)
      add(referral.referrer, REFERRER_REWARD, 'referral_reward', referral.id)
    end

    def add_referee_credit(referral)
      add(referral.referee, REFEREE_REWARD, 'referee_reward', referral.id)
    end
  end
end
