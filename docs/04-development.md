### Development

#### Running docker-compose

Install [docker-compose](https://docs.docker.com/compose/install/).

```
# Aliases
alias dcomp='/usr/local/bin/docker-compose'
alias dkr='/usr/bin/docker'

# docker-compose helpers
docker_compose_logs() {
  cd "$1"
  dcomp logs -f --tail=150
}
docker_compose_build() {
  cd "$1"
  dcomp build
}
docker_compose_up() {
  cd "$1"
  dcomp up -d --force-recreate
}
docker_compose_restart() {
  cd "$1"
  dcomp restart "$2"
}
docker_compose_cd() {
  cd "$1"
}
docker_compose_console() {
  cd "$1"
  dkr exec -it "$2" sh
}
docker_compose_test() {
  cd "$1"
  dcomp -f docker-compose.yml -f docker-compose.test.yml run "$2"
}

AMITREE_PATH="$HOME/git/amitree"
alias amitree_logs='docker_compose_logs "${AMITREE_PATH}"'
alias amitree_build='docker_compose_build "${AMITREE_PATH}"'
alias amitree_up='docker_compose_up "${AMITREE_PATH}"'
alias amitree_restart='docker_compose_restart "${AMITREE_PATH}" amitree-rails-api'
alias amitree_cd='docker_compose_cd "${AMITREE_PATH}"'
alias amitree_console='docker_compose_console "${AMITREE_PATH}" amitree-rails-api'
alias amitree_test='docker_compose_test "${AMITREE_PATH}" amitree-api-guard'
alias amitree_rswag='docker_compose_test "${AMITREE_PATH}" amitree-rswag'
```

Just put this into your `.bash_profile` and use `amitree_build && amitree_up` to run project.
- `amitree_logs` allows you to watch for any problems on containers during development / setup
- `amitree_restart` restarts the `rails-api` container
- `amitree_cd` allows you to quickly navigate to the project folder
- `amitree_console` opens ssh session on `rails-api` container
- `amitree_test` runs `guard` with `rspec` and `rubocop`. Press `enter` to run entire test suit.
- `amitree_rswag` generates API docs from the `spec/swagger` folder

#### Testing

Use `amitree_test` to run the `guard` session in your terminal. Type `exit` to close it. Press `enter` to run entire test suit.

#### Add / update a gem

```
# amitree_console
# bundle config unset frozen
# bundle install
# bundle cache
# exit
# amitree_build && amitree_up
```

[The reason](https://12factor.net/dev-prod-parity) why we need to ssh into docker container is simply because container could have a different OS environment from what you may have installed as your host OS.
There could be many developers working on the same project, using all kind of Ruby versions, OSes.
You don't want to deal with different platforms like `mingw`.
Everybody on the team must use same development runtime environment. Also soon there would be even less setup, allowing us to use [containerized development environments](https://code.visualstudio.com/docs/remote/containers).
It increases productivity and wouldn't let anyone to say "but it works on my machine".

#### Dotenv

If you need to override anything already specified on the `.env.development`, please read the [official docs](https://github.com/bkeepers/dotenv).

#### Documenting API

Use `amitree_rswag` to update `swagger/v1/swagger.json`. It uses `spec/swagger` to run tests and automagically populate example responses, however you should be writing example parameters to work nicely with Swagger-compatible tools like [Postman](https://www.postman.com/). See `spec/swagger/api/v1/users_request_spec.rb:22`.

#### Troubleshooting

Sometimes there could be problems 