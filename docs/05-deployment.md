### Deployment

#### Gitlab

For Gitlab CI/CD, everything is already configured, and have build / test / code quality / staging & production deployment pipelines running.

#### Environment variables that must be set on CI/CD

```
# all environments
DAST_DISABLED=true
K8S_SECRET_RAILS_MASTER_KEY="ask devs for their config/master.key"
K8S_SECRET_REDIS_PATH=redis://production-redis-master.#{project-namespace}.svc.cluster.local:6379/0

# production
ADDITIONAL_HOSTS=whatever.example.com
DB_INITIALIZE=cd /rails && RAILS_ENV=production bin/rails db:setup
DB_MIGRATE=cd /rails && RAILS_ENV=production bin/rails db:migrate
K8S_SECRET_API_BASE_URL=https://whatever.example.com
K8S_SECRET_RAILS_ENV=production
K8S_SECRET_REDIS_URL=redis://production-redis-master.#{project-namespace}.svc.cluster.local:6379/0
K8S_SECRET_FRONTEND_URL=https://whatever.example.com
```

For Gitlab, these are also necessary to set if you are self-hosting k8s cluster:

```
AUTO_DEVOPS_BUILD_IMAGE_EXTRA_ARGS="--network host"
KUBE_INGRESS_BASE_DOMAIN=your.base.domain.com
```

If you would like your application to work with your own domain and Let's Encrypt, it should have a wildcard DNS A record pointing at your cluster IP:

```
;; A Records
*.your.base.domain.com.	1	IN	A	112.204.1.23
```
