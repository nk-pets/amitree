### Architecture

#### Database scheme

First thing we have to do is to come up with a simple database scheme for our application. We'll use [QuickDBD](https://www.quickdatabasediagrams.com) for that purpose.

QuickDBD diagram:

```
User
-
id PK int
name string INDEX
email string UNIQUE

ReferralCode
-
id PK int
user_id int FK >- User.id
code string UNIQUE
invited int

Referral
-
id PK int
# A person who refers another person.
referral_code_id int FK >- ReferralCode.id
# A person who was referred by another person
user_id int FK >- User.id
status string

Credit
-
id PK int
user_id int FK >- User.id
balance int
amount int
reason string
referral_id FK -0 Referral.id
```

P.S. I haven't included `created_at`, `updated_at` fields in the diagram because we don't really need them at this architecture design stage.

Result:

![database-scheme](database-scheme.png)

A few thoughts behind this scheme:

- For `ReferralCode`, we would like to have a single record for every user for now. More possible if we would like to track referrals from different channels (email, social, etc).
- `ReferralCode.invited` is a field to account for our Referral program (see below).
- Every `Referral` have the `status` column to make sure we're processing them properly via Sidekiq.
- Idea behind `Credit` table is simple: use it as a logging solution, with the `balance` column changing over time. `amount` could be negative and positive. `reason` describes what happened to credits: referral program, assignment by admin, or spending.
- `Credit.referral_id` is an optional field, I think it would let us recalculate bonuses later and also keep more consistent database in the future.

#### Referral program

This one was the most tricky to figure out, as we don't want to spend time figuring out what should happen to `credits` and `referrals` tables during sign up.
From this requirement, I figured we should use Sidekiq / Redis stack to process referrals and also off-load some of the future jobs (emails, etc).
A few things we should take care of:

- Retry failed jobs.
- Use `Referral.status` field as a way to determine which steps need to be processed further.
- Make sure we don't create multiple credit records for the same referral.
- Make sure we account only for referral credit records (`reason: 'referral_reward'`) (having non-empty `credit.referral_id` and defined amount of referral records).

#### Credit system

Current credit balance should be always the last record for the User: `user.credits.last`.
Based on this rule, we can define our `Credit.add` method as:
- Take last known balance: `last_balance = where(user_id: user.id).last&.balance.to_i`
- Add amount to balance for the new record: `create(user: user, amount: amount, reason: reason, balance: last_balance + amount, referral_id: referral_id)`.
- `referral_id` may be `nil`.

There could be racing conditions in case multiple signups are going through in parallel. For that, we would have to lock our Credit record like:

```
Credit.transaction do
  last_credit = Credit.where(...).last
  last_credit.lock!
  new_credit = Credit.create(...)
end
```
