require 'swagger_helper'

RSpec.describe 'Referral Codes', type: :request do
  path '/api/v1/referral_code' do
    get 'Get a Referral code' do
      tags 'Referral'
      security [bearer_auth: []]

      let(:existing_user) { create(:user) }
      let(:existing_user_with_referral_code) { create(:user, :with_referral_code) }

      response 201, 'Referral code created and retrieved' do
        let!(:Authorization) { authenticated_header(existing_user).values.first }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test! do
          expect(response.body).to match(/referral_link/)
        end
      end
    end
  end
end
