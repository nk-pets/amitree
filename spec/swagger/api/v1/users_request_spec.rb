require 'swagger_helper'

RSpec.describe 'Users', type: :request do
  path '/api/v1/users' do
    post 'Create a user' do
      tags 'User'
      consumes 'application/json'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          user: {
            type: :object,
            properties: {
              email: { type: :string },
              name: { type: :string },
              password: { type: :string },
              password_confirmation: { type: :string },
              referral_code: { type: :string }
            },
            required: %w[email name password password_confirmation]
          }
        },
        example: {
          user: {
            email: 'john@example.net',
            name: 'John Doe',
            password: 'example',
            password_confirmation: 'example'
          }
        },
        required: %w[user]
      }

      response 201, 'User have been created successfully' do
        let(:user) do
          {
            user: {
              email: 'john@example.net',
              name: 'John Doe',
              password: 'example',
              password_confirmation: 'example'
            }
          }
        end

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test! do
          expect(json[:name]).to match(/John Doe/)
        end
      end

      response 400, 'Invalid email' do
        let(:user) do
          {
            user: {
              email: 'john@',
              name: 'John Doe',
              password: 'example',
              password_confirmation: 'example'
            }
          }
        end

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test! do
          expect(json[:error][:title]).to match(/Could not register a new user/)
        end
      end
    end
  end

  path '/api/v1/user' do
    get 'Show a user' do
      let(:user) { create(:user, name: 'Erich Wunsch') }
      tags 'User'
      security [bearer_auth: []]
      response 200, 'User have been found successfully' do
        let!(:Authorization) { authenticated_header(user).values.first }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test! do
          expect(json[:name]).to match(/Erich Wunsch/)
        end
      end

      response 404, 'User haven\'t been found' do
        let!(:Authorization) { 'Bearer invalid' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test! do
          expect(json[:error][:detail]).to match(/Provided Bearer token is not valid/)
        end
      end
    end
  end
end
