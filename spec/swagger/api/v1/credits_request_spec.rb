require 'swagger_helper'

RSpec.describe 'Credits', type: :request do
  path '/api/v1/credit' do
    get 'Get current Credit balance' do
      tags 'Credits'
      security [bearer_auth: []]

      let(:existing_user_with_credits) { create(:user, :with_credits, credit_count: 2) }

      response 200, 'Credits code created and retrieved' do
        let!(:Authorization) { authenticated_header(existing_user_with_credits).values.first }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test! do
          expect(response.body).to match(/balance/)
        end
      end
    end
  end

  path '/api/v1/credits' do
    get 'Get Credit history' do
      tags 'Credits'
      security [bearer_auth: []]

      let(:existing_user_with_credits) { create(:user, :with_credits, credit_count: 2) }

      response 200, 'Credits code created and retrieved' do
        let!(:Authorization) { authenticated_header(existing_user_with_credits).values.first }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test! do
          expect(response.body).to match(/balance/)
          expect(response.body).to match(/#{existing_user_with_credits.credits.first.balance}/)
          expect(response.body).to match(/#{existing_user_with_credits.credits.last.balance}/)
        end
      end
    end
  end
end
