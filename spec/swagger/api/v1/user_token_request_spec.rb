require 'swagger_helper'

RSpec.describe 'User Token', type: :request do
  path '/api/v1/auth/token' do
    post 'Create a JWT' do
      tags 'Auth'
      consumes 'application/json'
      parameter name: :auth, in: :body, schema: {
        type: :object,
        properties: {
          auth: {
            type: :object,
            properties: {
              email: { type: :string },
              password: { type: :string }
            },
            required: %w[email password]
          }
        },
        example: {
          auth: {
            email: 'john@example.net',
            password: 'example'
          }
        },
        required: %w[auth]
      }
      let!(:existing_user) do
        create(:user, email: 'john@example.net', password: 'example', password_confirmation: 'example')
      end

      response 201, 'Auth have been created successfully' do
        let(:auth) do
          {
            auth: {
              email: 'john@example.net',
              password: 'example'
            }
          }
        end

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test! do
          expect(response.body).to match(/jwt/)
        end
      end

      response 404, 'Invalid email/password combination' do
        let(:auth) do
          {
            auth: {
              email: 'kate@example.net',
              password: 'invalid'
            }
          }
        end

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test! do
          expect(response.body).to match(/Requested resource was not found/)
        end
      end
    end
  end

  path '/api/v1/user' do
    get 'Show a user' do
      let(:user) { create(:user, name: 'Erich Wunsch') }
      tags 'User'
      security [bearer_auth: []]
      response 200, 'User have been found successfully' do
        let!(:Authorization) { authenticated_header(user).values.first }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test! do
          expect(json[:name]).to match(/Erich Wunsch/)
        end
      end

      response 404, 'User haven\'t been found' do
        let!(:Authorization) { 'Bearer invalid' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test! do
          expect(json[:error][:detail]).to match(/Provided Bearer token is not valid/)
        end
      end
    end
  end
end
