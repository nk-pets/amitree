FactoryBot.define do
  factory :referral do
    user
    referral_code
    status { 'created' }
  end
end
