FactoryBot.define do
  factory :referral_code do
    user
    code { SecureRandom.hex }
    invited { 0 }
  end
end
