FactoryBot.define do
  factory :user do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    password { 'password' }
    password_confirmation { 'password' }

    trait :with_referral_code do
      after(:build) do |user|
        user.referral_code = build(:referral_code, user: user)
      end
    end

    trait :with_credits do
      transient do
        credit_count { 1 }
        credit_amount { 100 }
      end

      after(:create) do |user, evaluator|
        evaluator.credit_count.times do
          Credit.add(user, evaluator.credit_amount, 'referral_reward')
        end
      end
    end

    trait :with_referrals do
      transient do
        referral_count { 1 }
      end

      after(:build) do |user, evaluator|
        user.referral_code = build(:referral_code, user: user, invited: evaluator.referral_count)
      end
    end
  end
end
