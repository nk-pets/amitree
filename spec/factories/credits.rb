FactoryBot.define do
  factory :credit do
    user
    balance { 1000 }
    amount { 1000 }
    reason { 'referral_reward' }
    referral_id { nil }

    trait :with_referral do
      after(:build) do |credit|
        credit.referral = build(:referral)
      end
    end
  end
end
