require 'rails_helper'

RSpec.describe ReferralCreditWorker, type: :worker do
  it { is_expected.to be_retryable true }
  it { is_expected.to be_processed_in :referral }

  describe '.perform' do
    subject(:worker) { described_class.new }

    context 'when Referral created for a user without referrals' do
      let(:user_without_referrals) { create(:user, :with_referral_code) }
      let(:referral) { create(:referral, referral_code: user_without_referrals.referral_code) }

      before do
        expect_any_instance_of(Referral).to receive(:process_credit)
      end

      after(:each, :perform) do
        worker.perform(referral.id)
      end

      it 'changes referral status to "processing"', :perform do
        expect_any_instance_of(Referral).to receive(:processing!).and_call_original
      end

      it 'calls Credit.process_referral', :perform do
        expect(Credit).to receive(:process_referral).with(referral).and_call_original
      end

      it 'enqueues job with referral_id parameter' do
        described_class.perform_async(referral.id)
        expect(described_class).to have_enqueued_sidekiq_job(referral.id)
      end

      it 'creates one Credit entity' do
        expect { worker.perform(referral.id) }.to change { Credit.count }.by(1)
      end

      it 'changes credit balance for referee' do
        expect { worker.perform(referral.id) }
          .to change { referral.referee.credit_balance }.by(Credit::REFEREE_REWARD)
      end

      it 'does not change credit balance for referrer' do
        expect { worker.perform(referral.id) }.not_to change(referral.referrer, :credit_balance)
      end
    end

    context 'when Referral created for a user with 4 referrals' do
      let(:user_with_referrals) { create(:user, :with_referrals, referral_count: 4) }
      let(:referral) { create(:referral, referral_code: user_with_referrals.referral_code) }

      before do
        expect_any_instance_of(Referral).to receive(:process_credit)
      end

      after(:each, :perform) do
        worker.perform(referral.id)
      end

      it 'changes referral status to "processing"', :perform do
        expect_any_instance_of(Referral).to receive(:processing!).and_call_original
      end

      it 'calls Credit.process_referral', :perform do
        expect(Credit).to receive(:process_referral).with(referral).and_call_original
      end

      it 'enqueues job with referral_id parameter' do
        described_class.perform_async(referral.id)
        expect(described_class).to have_enqueued_sidekiq_job(referral.id)
      end

      it 'creates two Credit entities' do
        expect { worker.perform(referral.id) }.to change { Credit.count }.by(2)
      end

      it 'changes credit balance for referee' do
        expect { worker.perform(referral.id) }
          .to change { referral.referee.credit_balance }.by(Credit::REFEREE_REWARD)
      end

      it 'changes credit balance for referrer' do
        expect { worker.perform(referral.id) }
          .to change { referral.referrer.credit_balance }.by(Credit::REFERRER_REWARD)
      end
    end

    context 'when Referral created for a user with 5 referrals and one referral bonus' do
      let(:user_with_referrals) { create(:user, :with_referrals, referral_count: 4) }
      let(:credit) { create(:credit, :with_referral, user: user_with_referrals) }
      let(:referral) { credit.referral }

      before do
        expect_any_instance_of(Referral).to receive(:process_credit)
      end

      after(:each, :perform) do
        worker.perform(referral.id)
      end

      it 'changes referral status to "processing"', :perform do
        expect_any_instance_of(Referral).to receive(:processing!).and_call_original
      end

      it 'calls Credit.process_referral', :perform do
        expect(Credit).to receive(:process_referral).with(referral).and_call_original
      end

      it 'enqueues job with referral_id parameter' do
        described_class.perform_async(referral.id)
        expect(described_class).to have_enqueued_sidekiq_job(referral.id)
      end

      it 'creates two Credit entities' do
        # necessary to evaluate this one before the next expectation
        expect(credit.user).to eq(user_with_referrals)
        expect { worker.perform(referral.id) }.to change { Credit.count }.by(1)
      end

      it 'changes credit balance for referee' do
        expect { worker.perform(referral.id) }
          .to change { referral.referee.credit_balance }.by(Credit::REFEREE_REWARD)
      end

      it 'does not change credit balance for referrer' do
        expect { worker.perform(referral.id) }
          .to change { referral.referrer.credit_balance }.by(0)
      end
    end

    context 'when Referral not found' do
      let(:unexistent_referral_id) { non_existing_record_id }

      it 'raises an error' do
        expect { worker.perform(unexistent_referral_id) }
          .to raise_error "Referral #{unexistent_referral_id} was not found"
      end
    end

    context 'when Referral have different from "created" status' do
      let(:referral) { create(:referral, status: 'processing') }

      before do
        expect_any_instance_of(Referral).to receive(:process_credit)
      end

      it 'raises an error' do
        expect { worker.perform(referral.id) }
          .to raise_error 'Referral is already processing'
      end
    end
  end
end
