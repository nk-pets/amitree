require 'rails_helper'

RSpec.describe 'ReferralCodes', type: :request do
  describe '#create' do
    context 'when signed in as a valid user' do
      context 'when user have referral code' do
        let!(:valid_user) { create(:user, :with_referral_code) }
        let!(:auth_token) { authenticated_header(valid_user) }
        let!(:referral_code) { valid_user.referral_code.code }
        subject { get api_v1_referral_code_path, headers: auth_token }

        it_should_behave_like 'JSON response', :created do
          let(:matches) { ['referral_link', ENV['FRONTEND_URL'], referral_code] }
        end

        it 'does not change ReferralCode count' do
          expect { subject }.to change { ReferralCode.count }.by(0)
        end
      end

      context "when user doesn't have referral code" do
        let!(:valid_user) { create(:user) }
        let!(:auth_token) { authenticated_header(valid_user) }

        subject { get api_v1_referral_code_path, headers: auth_token }

        it_should_behave_like 'JSON response', :created do
          let(:matches) { ['referral_link', ENV['FRONTEND_URL']] }
        end

        it 'creates a new ReferralCode' do
          expect { subject }.to change { ReferralCode.count }.by(1)
        end
      end
    end

    context 'when invalid token provided' do
      subject { get api_v1_referral_code_path, headers: invalid_authentication_header }

      it_should_behave_like 'JSON response', :not_found do
        let(:matches) { ['Provided Bearer token is not valid'] }

        it { expect { subject }.to change { User.count }.by(0) }
      end
    end
  end
end
