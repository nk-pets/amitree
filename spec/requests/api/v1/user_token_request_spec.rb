require 'rails_helper'

RSpec.describe Api::V1::UserTokenController, type: :request do
  def authenticate_user(auth_params)
    post api_v1_token_path, params: { auth: auth_params }
  end

  describe '#create' do
    context 'valid authentication' do
      let!(:existing_user) { create(:user, password: 'example', password_confirmation: 'example') }

      subject do
        authenticate_user(
          {
            email: existing_user.email,
            password: 'example'
          }
        )
      end

      it_behaves_like 'JSON response', :created do
        let(:matches) { ['jwt'] }
      end

      it 'does not change User count' do
        expect { subject }.to change { User.count }.by(0)
      end
    end

    context 'invalid authentication' do
      let!(:existing_user) { create(:user, password: 'example', password_confirmation: 'example') }

      subject do
        authenticate_user(
          {
            email: existing_user.email,
            password: 'invalid'
          }
        )
      end

      it_behaves_like 'JSON response', :not_found do
        let(:matches) { ['Requested resource was not found'] }
      end

      it 'does not change User count' do
        expect { subject }.to change { User.count }.by(0)
      end
    end
  end
end
