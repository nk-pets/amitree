require 'rails_helper'

RSpec.describe Api::V1::CreditsController, type: :request do
  describe '#index' do
    context 'valid authentication' do
      let!(:valid_user_with_credits) { create(:user, :with_credits, credit_count: 2) }

      subject { get api_v1_credits_path, headers: authenticated_header(valid_user_with_credits) }

      it_behaves_like 'JSON response' do
        let(:matches) do
          [
            valid_user_with_credits.credits.last.balance,
            valid_user_with_credits.credits.first.balance,
            'balance',
            'amount'
          ]
        end
      end
    end

    context 'when user credits are empty' do
      let!(:valid_user_without_credits) { create(:user) }

      subject { get api_v1_credits_path, headers: authenticated_header(valid_user_without_credits) }

      it_behaves_like 'JSON response', :not_found do
        let(:matches) { ["Current user doesn't have any credits"] }
      end
    end

    context 'token valid before expiration' do
      let!(:valid_user_with_credits) { create(:user, :with_credits, credit_count: 2) }
      let!(:auth_token) { authenticated_header(valid_user_with_credits) }

      subject { get api_v1_credits_path, headers: auth_token }

      it_behaves_like 'JSON response' do
        before { Timecop.freeze(Knock.token_lifetime - 1.second) }
        let(:matches) do
          [
            valid_user_with_credits.credits.last.balance,
            valid_user_with_credits.credits.first.balance,
            'balance',
            'amount'
          ]
        end
      end
    end

    context 'invalid token' do
      subject { get api_v1_credits_path, headers: invalid_authentication_header }

      it_behaves_like 'JSON response', :not_found do
        let(:matches) { ['Provided Bearer token is not valid'] }
      end
    end

    context 'token expired' do
      let!(:valid_user_with_credits) { create(:user, :with_credits, credit_count: 2) }
      let!(:auth_token) { authenticated_header(valid_user_with_credits) }

      subject { get api_v1_credits_path, headers: auth_token }

      it_behaves_like 'JSON response', :not_found do
        before { Timecop.freeze(Knock.token_lifetime + 1.second) }
        let(:matches) { ['Provided Bearer token is not valid'] }
      end
    end
  end

  describe '#show' do
    context 'valid authentication' do
      let!(:valid_user_with_credits) { create(:user, :with_credits) }

      subject { get api_v1_credit_path, headers: authenticated_header(valid_user_with_credits) }

      it_behaves_like 'JSON response' do
        let(:matches) { [valid_user_with_credits.credits.last.balance, 'balance', 'amount'] }
      end
    end

    context 'when user credits are empty' do
      let!(:valid_user_without_credits) { create(:user) }

      subject { get api_v1_credit_path, headers: authenticated_header(valid_user_without_credits) }

      it_behaves_like 'JSON response', :not_found do
        let(:matches) { ["Current user doesn't have any credits"] }
      end
    end

    context 'token valid before expiration' do
      let!(:valid_user_with_credits) { create(:user, :with_credits) }
      let!(:auth_token) { authenticated_header(valid_user_with_credits) }

      subject { get api_v1_credit_path, headers: auth_token }

      it_behaves_like 'JSON response' do
        before { Timecop.freeze(Knock.token_lifetime - 1.second) }
        let(:matches) { [valid_user_with_credits.credits.last.balance] }
      end
    end

    context 'invalid token' do
      subject { get api_v1_credit_path, headers: invalid_authentication_header }

      it_behaves_like 'JSON response', :not_found do
        let(:matches) { ['Provided Bearer token is not valid'] }
      end
    end

    context 'token expired' do
      let!(:valid_user) { create(:user) }
      let!(:auth_token) { authenticated_header(valid_user) }

      subject { get api_v1_credit_path, headers: auth_token }

      it_behaves_like 'JSON response', :not_found do
        before { Timecop.freeze(Knock.token_lifetime + 1.second) }
        let(:matches) { ['Provided Bearer token is not valid'] }
      end
    end
  end
end
