require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :request do
  def create_new_user(user_params)
    post api_v1_users_path, params: {
      user: user_params
    }
  end

  describe '#create' do
    context 'valid registration' do
      subject do
        create_new_user(
          {
            email: 'john@example.net',
            name: 'John Doe',
            password: 'example',
            password_confirmation: 'example'
          }
        )
      end

      it_behaves_like 'JSON response', :created do
        let(:matches) { ['john@example.net'] }
      end

      it 'changes User count by 1' do
        expect { subject }.to change { User.count }.by(1)
      end
    end

    context 'having existing user with a referral code' do
      let!(:existing_user) { create(:user, :with_referral_code) }

      context 'valid registration with referral code' do
        subject do
          create_new_user(
            {
              email: 'john@example.net',
              name: 'John Referral',
              password: 'example',
              password_confirmation: 'example',
              referral_code: existing_user.referral_code.code
            }
          )
        end

        it_behaves_like 'JSON response', :created do
          let(:matches) { ['john@example.net'] }
        end

        it 'changes User count by 1' do
          expect { subject }.to change { User.count }.by(1)
        end

        it 'changes Referral count by 1' do
          expect { subject }.to change { Referral.count }.by(1)
        end

        it 'changes ReferralCode invited amount by 1' do
          expect { subject }.to change { existing_user.reload.referral_code.invited }.by(1)
        end
      end

      context 'valid registration with invalid referral code' do
        subject do
          create_new_user(
            {
              email: 'john@example.net',
              name: 'John Referral',
              password: 'example',
              password_confirmation: 'example',
              referral_code: 'invalid code'
            }
          )
        end

        it_behaves_like 'JSON response', :bad_request do
          let(:matches) { ['Please provide a valid referral code'] }
        end

        it 'does not change User count' do
          expect { subject }.not_to change(User, :count)
        end

        it 'does not change Referral count' do
          expect { subject }.not_to change(Referral, :count)
        end
      end
    end

    context 'invalid email' do
      before(:each) do
        create_new_user({ email: 'john@', name: 'John Doe' })
      end

      it 'responds with proper error message' do
        expect(response.body).to match('Email is invalid')
      end
    end

    context 'empty name' do
      before(:each) do
        create_new_user({ email: 'john@example.net', name: '' })
      end

      it 'responds with proper error message' do
        expect(response.body).to match("Name can't be blank")
      end
    end

    context 'empty email' do
      before(:each) do
        create_new_user({ email: '', name: 'John Doe' })
      end

      it 'responds with proper error message' do
        expect(response.body).to match("Email can't be blank")
      end
    end
  end

  describe '#show' do
    context 'valid authentication' do
      let!(:valid_user) { create(:user) }

      subject { get api_v1_user_path, headers: authenticated_header(valid_user) }

      it_behaves_like 'JSON response' do
        let(:matches) { [valid_user.email] }
      end

      it 'does not change User count' do
        expect { subject }.to change { User.count }.by(0)
      end
    end

    context 'token valid before expiration' do
      let!(:valid_user) { create(:user) }
      let!(:auth_token) { authenticated_header(valid_user) }

      subject { get api_v1_user_path, headers: auth_token }

      it_behaves_like 'JSON response' do
        before { Timecop.freeze(Knock.token_lifetime - 1.second) }
        let(:matches) { [valid_user.email] }

        it { expect { subject }.to change { User.count }.by(0) }
      end
    end

    context 'invalid token' do
      subject { get api_v1_user_path, headers: invalid_authentication_header }

      it_behaves_like 'JSON response', :not_found do
        let(:matches) { ['Provided Bearer token is not valid'] }
      end

      it 'does not change User count' do
        expect { subject }.to change { User.count }.by(0)
      end
    end

    context 'token expired' do
      let!(:valid_user) { create(:user) }
      let!(:auth_token) { authenticated_header(valid_user) }

      subject { get api_v1_user_path, headers: auth_token }

      it_behaves_like 'JSON response', :not_found do
        before { Timecop.freeze(Knock.token_lifetime + 1.second) }
        let(:matches) { ['Provided Bearer token is not valid'] }

        it { expect { subject }.to change { User.count }.by(0) }
      end
    end
  end
end
