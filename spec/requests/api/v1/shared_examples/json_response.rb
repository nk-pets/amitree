require 'rails_helper'

RSpec.shared_examples 'JSON response' do |status = :ok|
  context 'JSON response' do
    before(:each) { subject }

    it 'performs the expected side-effects' do
      aggregate_failures do
        expect { json }.not_to raise_error

        if defined?(matches)
          matches.each do |match|
            expect(response.body).to match(/#{match}/)
          end
        end

        expect(response).to have_http_status(status)
      end
    end
  end
end
