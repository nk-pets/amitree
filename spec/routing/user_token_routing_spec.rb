require 'rails_helper'

RSpec.describe Api::V1::UserTokenController, type: :routing do
  it 'routes to #create' do
    expect(post(api_v1_token_path)).to route_to('api/v1/user_token#create', format: :json)
  end
end
