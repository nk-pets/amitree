require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :routing do
  it 'routes to #create' do
    expect(post(api_v1_users_path)).to route_to('api/v1/users#create', format: :json)
  end

  it 'routes to #show' do
    expect(get(api_v1_user_path)).to route_to('api/v1/users#show', format: :json)
  end
end
