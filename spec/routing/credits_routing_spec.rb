require 'rails_helper'

RSpec.describe Api::V1::CreditsController, type: :routing do
  it 'routes to #index' do
    expect(get(api_v1_credits_path)).to route_to('api/v1/credits#index', format: :json)
  end

  it 'routes to #show' do
    expect(get(api_v1_credit_path)).to route_to('api/v1/credits#show', format: :json)
  end
end
