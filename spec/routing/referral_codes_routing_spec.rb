require 'rails_helper'

RSpec.describe Api::V1::ReferralCodesController, type: :routing do
  it 'routes to #show' do
    expect(get(api_v1_referral_code_path)).to route_to('api/v1/referral_codes#show', format: :json)
  end
end
