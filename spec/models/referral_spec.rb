require 'rails_helper'

RSpec.describe Referral, type: :model do
  describe '.link' do
    context 'having valid users and referral code' do
      let(:user_with_referral_code) { create(:user, :with_referral_code) }
      let(:new_user) { create(:user) }

      context 'valid parameters' do
        subject do
          -> { Referral.link(new_user, user_with_referral_code.referral_code.code) }
        end

        it 'creates a new referral' do
          expect(subject).to change { Referral.count }.by(1)
        end

        it 'increases invited count by 1' do
          expect(subject).to change { user_with_referral_code.reload.referral_code.invited }.by(1)
        end
      end

      context 'invalid user' do
        subject do
          -> { Referral.link(nil, user_with_referral_code.referral_code.code) }
        end

        it 'raises an error' do
          expect(subject).to raise_error ApiExceptions::ReferralError::MissingUserError
        end

        it 'does not create a new referral' do
          expect { subject }.not_to change(Referral, :count)
        end

        it 'does not increases the invited count by 1' do
          expect { subject }.not_to change(user_with_referral_code.reload.referral_code, :invited)
        end
      end

      context 'invalid referral code' do
        subject do
          -> { Referral.link(new_user, 'invalid referral code') }
        end

        it 'raises an error' do
          expect(subject).to raise_error ApiExceptions::ReferralError::InvalidReferralCodeError
        end

        it 'does not create a new referral' do
          expect { subject }.not_to change(Referral, :count)
        end

        it 'does not increases the invited count by 1' do
          expect { subject }.not_to change(user_with_referral_code.reload.referral_code, :invited)
        end
      end
    end
  end
end
