require 'rails_helper'

RSpec.describe Credit, type: :model do
  describe 'validations' do
    it { should validate_inclusion_of(:reason).in_array(Credit::REASONS) }
    it { should validate_presence_of(:balance) }
    it { should validate_presence_of(:amount) }
  end

  describe '.process_referral' do
    context 'when referral with a user without referrals exist' do
      let(:user_without_referrals) { create(:user, :with_referral_code) }
      let(:referral) { create(:referral, referral_code: user_without_referrals.referral_code) }

      subject { -> { Credit.process_referral(referral) } }

      it { is_expected.not_to raise_error }
      it { expect(subject).to change { Credit.count }.by(1) }
    end

    context 'when Referral created for a user with 4 referrals' do
      let(:user_with_referrals) { create(:user, :with_referrals, referral_count: 4) }
      let(:referral) { create(:referral, referral_code: user_with_referrals.referral_code) }

      subject { -> { Credit.process_referral(referral) } }

      it { is_expected.not_to raise_error }
      it { expect(subject).to change { Credit.count }.by(2) }
    end

    context 'when referral is nil' do
      subject { -> { Credit.process_referral(nil) } }

      it { is_expected.to raise_error StandardError, 'referral is not an instance of the Referral class' }
    end
  end

  describe '.add' do
    # skip
  end
end
