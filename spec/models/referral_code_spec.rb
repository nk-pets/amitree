require 'rails_helper'

RSpec.describe ReferralCode, type: :model do
  describe '.get' do
    context 'user without referral code' do
      let!(:user_without_code) { create(:user) }

      it 'creates a new referral code' do
        expect { ReferralCode.get(user_without_code) }.to change { ReferralCode.count }.by(1)
      end
    end

    context 'user with referral code' do
      let!(:user_with_code) { create(:user, :with_referral_code) }

      it 'does not create a new referral code' do
        expect { ReferralCode.get(user_with_code) }.to change { ReferralCode.count }.by(0)
      end
    end
  end

  describe '#referral_link' do
    let(:referral_code) { create(:referral_code) }
    subject { referral_code.referral_link }

    it 'includes the frontend url' do
      expect(subject).to match(/#{ENV['FRONTEND_URL']}/)
    end

    it 'includes the referral code' do
      expect(subject).to match(/#{referral_code.code}/)
    end
  end
end
