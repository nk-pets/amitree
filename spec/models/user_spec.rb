require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email) }
  end

  describe '.register' do
    context 'valid parameters' do
      subject do
        lambda {
          User.register(
            {
              email: 'john@example.net',
              name: 'John Doe',
              password: 'example',
              password_confirmation: 'example'
            }
          )
        }
      end

      it 'creates a new user' do
        expect(subject).to change { User.count }.by(1)
      end
    end

    context 'invalid email' do
      subject do
        -> { User.register({ name: 'John Doe', email: 'john@' }) }
      end

      it 'raises an error' do
        expect(subject).to raise_error(ActiveRecord::RecordInvalid, /Email is invalid/)
      end
    end

    context 'empty email' do
      subject do
        -> { User.register({ name: 'John Doe', email: '' }) }
      end

      it 'raises an error' do
        expect(subject).to raise_error(ActiveRecord::RecordInvalid, /Email can't be blank/)
      end
    end

    context 'empty name' do
      subject do
        -> { User.register({ name: '', email: 'john@example.net' }) }
      end

      it 'raises an error' do
        expect(subject).to raise_error(ActiveRecord::RecordInvalid, /Name can't be blank/)
      end
    end
  end
end
