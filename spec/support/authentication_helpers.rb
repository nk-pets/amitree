module Authentication
  module Helpers
    def authenticated_header(user)
      token = Knock::AuthToken.new(payload: { sub: user.id }).token

      { 'Authorization': "Bearer #{token}" }
    end

    def invalid_authentication_header
      { 'Authorization': 'Bearer invalid_token' }
    end
  end
end
