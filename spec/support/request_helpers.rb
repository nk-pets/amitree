module Request
  module Helpers
    def json
      json = JSON.parse(response.body)
      if json.is_a? Array
        json.map(&:deep_symbolize_keys)
      else
        json.deep_symbolize_keys
      end
    end
  end
end
