module NonExistingRecords
  module Helpers
    # Max value for PG `serial` type: https://www.postgresql.org/docs/12/datatype-numeric.html
    ACTIVE_MODEL_INTEGER_MAX = 2_147_483_647

    def non_existing_record_id
      ACTIVE_MODEL_INTEGER_MAX
    end
  end
end
